<?php
session_start();
$status_user=$_SESSION['username'];
if (!isset($_SESSION['username'])){
header ("location:login.php");
}
include 'koneksi.php';
?>


<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />


	<title>Data Fire History </title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!--  Paper Dashboard core CSS    -->

    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>

    <!-- Buat Datepicker -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    

    <!-- CSS untuk datatable -->

   <link href=" https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" />
   <link href=" https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
   
   <!-- CSS Buat bootstrap -->

   <link rel="stylesheet" href="datatables/css/bootstrap.min.css" />

    <!--  Fonts and icons     -->

    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">
		

    <!-- Jquery Core -->

    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    
    <!-- Javascript Datatable -->

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
 
    <!-- Datepicker -->
    
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    
    <!-- Javascript Papper -->
    
    <script src="assets/js/paper-dashboard.js"></script>
    <script src="assets/js/demo.js"></script>
    <script src="assets/js/bootstrap-checkbox-radio.js"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="index.php" class="simple-text">
                Fire Alarm </br>
										<b><u><?php $status_user ?></u></b>
                </a>
            </div>

            <ul class="nav">
                      <li>
                    <a href="index.php">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>


					<li class="active">
                    <a href="catatan_kebakaran.php">
                        <i class="fa fa-history"></i>
                        <p>Fire History</p>
                    </a>
                </li>
								<li>
                    <a href="info.php">
                        <i class="ti-info-alt"></i>
                        <p>About Me</p>
                    </a>
                </li>

            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="catatan_kebakaran.php">Fire History</a>
                </div>

                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">

						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-cog fa-fw"></i> <i class="fa fa-caret-down"></i>
						</a>
                              <ul class="dropdown-menu dropdown-user">
                                <li><a href="admin/manage-user/"><i class="fa fa-user fa-fw"></i>Manage User</a></li>
                                <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
                              </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


    <div class="container-fluid">
        <div class="table-responsive">
         <br />
        
	     <div class="row">
	      <div class="input-daterange">
	       <div class="col-md-4  col-md-offset-1">
	        <input type="text" placeholder="Masukan Tanggal Awal" name="start_date" id="start_date" class="form-control " />
	       </div>
	       <div class="col-md-4">
	        <input type="text" placeholder="Masukan Tanggal Akhir" name="end_date" id="end_date" class="form-control " />
	       </div>
	      </div>
	      <div class="col-md-2">
	       <input type="button" name="search" id="search" value="Search" class="btn btn-info" />
	      </div>
         </div>

         <br />
    
	     <table id="order_data" class="display nowrap" style="width:100%">
	      <thead>
	       <tr>
	        <th>History ID</th>
	        <th>Ruangan</th>
	        <th>Status</th>
	        <th>Tanggal</th>
	        <th>Jam</th>
	       </tr>
	      </thead>
	     </table>
        </div>
            </div>
		</div>
</body>



</html>


<script type="text/javascript" language="javascript" >
$(document).ready(function(){

 $('.input-daterange').datepicker({
  todayBtn:'linked',
  format: "yyyy-mm-dd",
  autoclose: true
 });

 fetch_data('no');

 function fetch_data(is_date_search, start_date='', end_date='')
 {
  var dataTable = $('#order_data').DataTable({
  
   "processing" : true,
   "serverSide" : true,
   "order" : [],
   dom: 'Bfrtip',
   buttons: [
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL'
            },

            'copy', 'csv', 'excel',  'print'
        ], 
    scrollY : '300px', 
   "ajax" : {
    url:"fetch-test.php",
    type:"POST",
  
    data:{
     is_date_search:is_date_search, start_date:start_date, end_date:end_date
    }
   }
  });
 }

 $('#search').click(function(){
  var start_date = $('#start_date').val();
  var end_date = $('#end_date').val();
  if(start_date != '' && end_date !='')
  {
   $('#order_data').DataTable().destroy();
   fetch_data('yes', start_date, end_date);
  }
  else
  {
   alert("Both Date is Required");
  }
 });

});
</script>
