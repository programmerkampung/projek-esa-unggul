<?php
session_start();
if (!isset($_SESSION['username'])){
header ("location:login.php");
}
include 'koneksi.php';
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Dashboard | Fire Alarm</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />
		<link href="assets/css/style.css" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="index.php" class="simple-text">
                    Fire Alarm
                </a>
            </div>

            <ul class="nav">
                      <li>
                    <a href="index.php">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>

          
					<li class="active">
                    <a href="catatan_kebakaran.php">
                        <i class="fa fa-history"></i>
                        <p>Fire History</p>
                    </a>
                </li>
								<li>
                    <a href="info.php">
                        <i class="ti-info-alt"></i>
                        <p>About Me</p>
                    </a>
                </li>

            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="catatan_kebakaran.php">Fire History</a>
                </div>

                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">

						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-cog fa-fw"></i> <i class="fa fa-caret-down"></i>
						</a>
                              <ul class="dropdown-menu dropdown-user">
                                <li><a href="#"><i class="fa fa-user fa-fw"></i>Manage User</a></li>
                                <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
                              </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
							
							<table class="table table-striped">                     
								 <div class="table responsive">
								     <thead>
											<tr>
								           <th>No</th>
								           <th>Ruangan</th>
								           <th>Status</th>
							            <th>Tanggal</th>
							             <th>Jam</th>
						          </tr>
											</thead>
											<tbody>
														<?php
														$result=mysqli_query($con,"select * from catatan_kebakaran;");
														
														if ($result->num_rows > 0) {
															
																while($row = $result->fetch_assoc()) {
																		echo '<tr>
																							<td scope="row">' . $row["no"]. '</td>
																							<td>' . $row["ruangan"] .'</td>
																							<td> '.$row["status"] .'</td>
																							<td> '.$row["tanggal"] .'</td>
																							<td> '.$row["jam"] .'</td>
																						</tr>';
														
																}
														} else {
																echo "0 results";
														} 
														?>
											 </tbody>
									</div>
							</table>
						</div>	
				</div>
</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio.js"></script>

		<script src="assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>
	
	<!-- Penambahan untuk Date Search -->
	
  <script src="vendor/jquery/dataTables.min.js"></script>
  <script src="vendor/bootstrap/js/dataTables.min.js"></script>
  <link rel="stylesheet" href="vendor/bootstrap/css/datepicker.css" />
  <script src="vendor/bootstrap/js/datepicker.js"></script>


</html>
