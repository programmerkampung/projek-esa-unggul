<?php
session_start();
include '../koneksi.php';

if (!isset($_SESSION['username'])){
header ("location:../login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="../assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Dashboard | Fire Alarm</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="../assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="../assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../assets/css/demo.css" rel="stylesheet" />
		<link href="../assets/css/style.css" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="../assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    Fire Alarm </br>
										<b><u>Admin</u></b>
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="index.php">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>

                <li>
                    <a href="../catatan_kebakaran.php">
                        <i class="fa fa-history"></i>
                        <p>Fire History</p>
                    </a>
                </li>
								<li>
                    <a href="../info.php">
                        <i class="ti-info-alt"></i>
                        <p>About Me</p>
                    </a>
                </li>

            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Dashboard</a>
                </div>

                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">

						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-cog fa-fw"></i> <i class="fa fa-caret-down"></i>
						</a>
                              <ul class="dropdown-menu dropdown-user">
                                <li><a href="manage-user"><i class="fa fa-user fa-fw"></i>Manage User</a></li>
                                <li><a href="../logout.php"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
                              </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">

              <div class="row">
    		                <div class="col-lg-4 col-md-6">
    		                    <div id="room1" class="panel panel-success">
    		                        <div class="panel-heading">
    		                             <div class="row">
    		                                <div class="col-xs-3">
    		                                    <i id="face-room1" class="fa fa-thumbs-up fa-5x"></i>
    		                                </div>
    		                                <div class="col-xs-9 text-right">
    		                                    <div class="text-right"><h4 id="ket-room1">SAFE</h4></div>
    		                                    <div><h3>Room 01</h3></div>
    		                                </div>
    		                            </div>
    		                        </div>

																       <div class="panel-footer">
																					 <div class="text-center"><b>Fire Detector</b></div>
																			</div>

    		                    </div>
    		                </div>

                <div class="col-lg-4 col-md-6">
                    <div id="room2" class="panel panel-success">
                        <div class="panel-heading">
                             <div class="row">
                                <div class="col-xs-3">
                                    <i id="face-room2" class="fa fa-thumbs-up fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="text-right"><h4 id="ket-room2">SAFE</h4></div>
                                    <div><h3>Room 01</h3></div>
                                </div>
                            </div>
                        </div>

    		                            <div class="panel-footer panel-footer-info">
																				 <div class="text-center"><b>Smoke Detector</b></div>
    		                            </div>
                    </div>
                </div>




                 <!-- Pemberitahuan Warning  -->

          <div id="show-warning" class="row">
          	<div class="col-sm-12">
<!--           	PAKAI CLASS HIDDEN UNTUK MENYEMBUNYIKAN -->
               <div id="warning" class="hidden alert alert-danger text-center">

				<span id="berkedip" class="text-dark">
				<h1>WARNING<i class="fa"></i></h1></span>
				 <script>
                setInterval(function(){
                     $('#warning').toggle();
                },1000);
                </script>
                <p class="text-dark" id="info">Fire Has Been Detect</p>
              </div>
            </div>
          </div>


        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, Code With <i class="fa fa-heart heart"></i> by <a href="#!"> Me</a>
                </div>
            </div>
        </footer>

    </div>
</div>


</body>

<audio id="myAudio" loop>
  <source src="../sound/alarm.wav" type="audio/wav">
  Your browser does not support the audio element.
</audio>

   <script type="text/javascript">
	 var x = document.getElementById("myAudio");


	function play() {
x.play();
}

	function statusFire() {

  $.ajax({
    url:"../check_status.php",
    success:function(data) {

			var result = data.split(",");
			var status_sensor=result[0];
			var status_insert=result[1];

		if(status_sensor==0){

			$('#room1').removeClass();
			$('#room2').removeClass();
			$('#warning').removeClass();
			$('#face-room1').removeClass();
			$('#face-room2').removeClass();

			$('#room1').addClass('panel panel-success');
			$('#room2').addClass('panel panel-success');
 			$('#warning').addClass('hidden jumbotron text-center');
 			$('#ket-room1').html('SAFE');
 			$('#ket-room2').html('SAFE');
			$('#face-room1').addClass('fa fa-thumbs-up fa-5x');
			$('#face-room2').addClass('fa fa-thumbs-up fa-5x');

          }
       else if(status_sensor==1){
				play();
     		$('#room1').removeClass();
 			$('#room2').removeClass();
 			$('#warning').removeClass();
			$('#face-room1').removeClass();
			$('#face-room2').removeClass();

 			$('#room1').addClass('panel panel-danger');
 			$('#room2').addClass('panel panel-success');
 			$('#warning').addClass('jumbotron alert alert-danger text-center');
 			$('#ket-room1').html('Fire Detected');
 			$('#ket-room2').html('SAFE');
			$('#face-room1').addClass('fa fa-fire fa-5x');
			$('#face-room2').addClass('fa fa-thumbs-up fa-5x');


			if(status_insert==1){

				$.ajax({
					type: "POST",
					url: 'insert_history.php',
					data: {ruangan: 'Room 1', status: 'KEBAKARAN'},
					success: function(data){

					}
					});
			}

           }
       else if(status_sensor==2){
					play();

     	$('#room1').removeClass();
 			$('#room2').removeClass();
 			$('#warning').removeClass();
 			$('#face-room1').removeClass();
			$('#face-room2').removeClass();

 			$('#room1').addClass('panel panel-success');
 			$('#room2').addClass('panel panel-danger');
 			$('#warning').addClass('jumbotron alert alert-danger text-center');
 			$('#ket-room1').html('SAFE');
 			$('#ket-room2').html('Smoke Detected');
			$('#info').html('Smoke Has Been Detected');
			$('#face-room1').addClass('fa fa-thumbs-up fa-5x');
			$('#face-room2').addClass('fa fa-cloud fa-5x');

				if(status_insert==1){

				$.ajax({
					type: "POST",
					url: '../insert_history.php',
					data: {ruangan: 'Room 1', status: 'ASAP'},
					success: function(data){


							}
					});
			}

       }
       else {
			play();
     		$('#room1').removeClass();
 			$('#room2').removeClass();
 			$('#warning').removeClass();
			$('#face-room1').removeClass();
			$('#face-room2').removeClass();

 			$('#room1').addClass('panel panel-danger');
 			$('#room2').addClass('panel panel-danger');
 			$('#warning').addClass('jumbotron alert alert-danger text-center');
 			$('#ket-room1').html('Fire Detected');
 			$('#ket-room2').html('Smoke Detected');
			$('#info').html('Smoke and Fire Has Been Detected');
			$('#face-room1').addClass('fa fa-fire fa-5x');
			$('#face-room2').addClass('fa fa-cloud fa-5x');

			if(status_insert==1){

				$.ajax({
					type: "POST",
					url: '../insert_history.php',
					data: {ruangan: 'Room 1', status: 'KEBAKARAN DAN ASAP'},
					success: function(data){


							}
					});
			}


          }
    }
  });
}




    setInterval(function(){
        statusFire();// this will run after every 1 seconds
    }, 1000);

	</script>

    <!--   Core JS Files   -->
    <script src="../assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="../assets/js/bootstrap-checkbox-radio.js"></script>

		<script src="../assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="../assets/js/demo.js"></script>


</html>
